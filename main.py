import time
import PySimpleGUI as sg
from io import StringIO
import pandas as pd
import urllib
import glob
import os
import zipfile
import requests as re 
from bs4 import BeautifulSoup
import csv
import pyautogui
from openpyxl import Workbook
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import sys

root = str(os.getcwd())
config = {
    'links':{
        'sistax_login':'https://cockpit.systax.com.br/Default.aspx?ReturnUrl=%2fCockpit%2fPrincipal.aspx',
        'sistax_principal':'https://cockpit.systax.com.br/Cockpit/Principal.aspx',
        'sistax_exportar':'https://cockpit.systax.com.br/Admin/Exporta_Regra.aspx',
        'egs':'https://app.egssistemas.com.br/login'
    },    
    'credentials':{
        'sistax_username':'fbarros@merqueo.com',
        'sistax_password':'Q31922',
        'egs_username':'MERQUEO',
        'egs_access_key':'54067',
        'egs_password':'123456'
    },
    'paths':{
        'input':root+'\\sheets\\input',
        'output':root+'\\sheets\\output\\',
        'logs':root+'\\logs\\',
        'selenium':root+'\\chromedriver.exe',
        'downloads':root+'\\downloads\\'
    },
    'vars':{
        'sistax_sheet_name':''
    }
}

def logger(function_name, msg):
    path_log = config['paths']['logs'] + 'log-' + time.strftime('%d%m%Y') + '.txt'
    with open(path_log, encoding='UTF-8', mode='a') as log:
       log.write(f"{time.strftime('%d/%m/%Y %H:%M:%S')} - [ {function_name} ] - {msg}\n")

def scan_directory(path:str,extension:str):
    arr_files = glob.glob(path+'\*.'+extension)
    if len(arr_files) > 1:
        print( 'Somente é possível conter um arquivo csv na pasta de entrada.')
        return 0
    else:
        return arr_files[0]

def move_from_downloads_to_input():
    # CHECKING DOWNLOAD FILES
    arr_files = glob.glob(config['paths']['downloads']+'\*.zip')
    print('arr_files',arr_files)

    # SCANNING FILES
    for item in arr_files:
        if '_sistax_' in item:
            target_folder = config['paths']['input']
            # WIPING INPUT DIRECTORY
            input_files = glob.glob(config['paths']['input']+'\*.*')
            if len(input_files) > 0:
                for old_file in input_files:
                    os.remove(old_file)
                    print(f'Removed: {old_file} ')

            # UNPACK/TRANSFER TO INPUT DIRECTORY
            with zipfile.ZipFile(item,'r') as zip_ref:
                zip_ref.extractall(target_folder)
                print('zip file unpacked!')

            # REMOVING THE .ZIP FILE FROM DOWNLOADS FILE
            os.remove(item)

            print('transfered!')
            break

def get_query(drv, query):
    '''Read SQL query or database table into a DataFrame
    parameters
    server: the merqueo server  to consult ('prod', 'shiny', 'dwh').
    query: str SQL query to be executed or a table name.'''
    data = re.get('http://wow_apps.mejoracontinua-merqueo.com/get_query',
                  params={'query': urllib.parse.quote(query),
                          'server': drv})
    try:
        result = pd.read_json(StringIO(str(data.content,'utf-8')))
    except ValueError:
        result = data.content
    return result

def send_query(data, table_name):
    '''Write DataFrame into mejora-continua database
    parameters
    table: the name of the table to write on.
    data: DataFrame containing the data to write. (must have the same names as the destiny table)
    method (optional): Writing method if the table exist ('fail',‘replace’,'append'). The default method is append.'''
    re.post('http://wow_apps.mejoracontinua-merqueo.com/send_query',
            data={'json': data.to_json(orient='records'),
                  'table_name': table_name})

def download_sheet_from_sistax():
    # INITIALIZE SELENIUM
    executable_path = config['paths']['selenium']
    download_path = config['paths']['downloads']

    options = webdriver.ChromeOptions() 
    prefs = {"profile.default_content_settings.popups": 0,
             "download.default_directory": download_path,
             "directory_upgrade": True}
    options.add_experimental_option("prefs", prefs)

    chrome = webdriver.Chrome(executable_path=executable_path,options=options)
    chrome.maximize_window()

    # LOGIN PAGE
    sistax_link = config['links']['sistax_login']
    chrome.get(sistax_link)
    wait_page_load = WebDriverWait(chrome, 10).until(EC.presence_of_element_located((By.XPATH, "//*[@id='txtEmail']")))

    user = chrome.find_element_by_id('txtEmail')
    user.send_keys(config['credentials']['sistax_username'])

    password = chrome.find_element_by_id('txtSenha')
    password.send_keys(config['credentials']['sistax_password'])
   
    chrome.execute_script('return document.querySelector("#btnEntrar").click()')
   
    # COCKPIT PAGE
    wait_page_load = WebDriverWait(chrome, 10).until(EC.presence_of_element_located((By.XPATH, '//*[@id="lblT_ConsultaRegrasTributarias"]')))

    # EXPORTAR REGRAS PAGE
    chrome.get('https://cockpit.systax.com.br/Admin/Exporta_Regra.aspx')
    wait_page_load = WebDriverWait(chrome, 10).until(EC.presence_of_element_located((By.XPATH, "//*[@id='CPH_txtMAT_DESC']")))
    id_cenario = chrome.find_element_by_id('CPH_txtIDCenario')
    id_cenario.send_keys('2218069')
    chrome.execute_script('return document.querySelector("#CPH_btnExportar").click()')
    wait_page_load = WebDriverWait(chrome, 10).until(EC.presence_of_element_located((By.XPATH, '//*[@id="popup_ok"]')))
    config['vars']['sistax_sheet_name'] = f"{time.strftime('%d%m%Y %H%M%S')} sistax"
    pyautogui.write(config['vars']['sistax_sheet_name'])
    chrome.execute_script('document.querySelector("#popup_ok").click()')
    wait_page_load = WebDriverWait(chrome, 10).until(EC.presence_of_element_located((By.XPATH, '//*[@id="divArquivos"]/div[2]/table/tbody/tr/td[4]')))
    print('ENTREI NO FOR')
    for x in range(60):
        status = chrome.find_element_by_xpath('//*[@id="divArquivos"]/div[2]/table/tbody/tr/td[5]').get_attribute('innerText')
        print(status)
        if status == 'Concluído':
            try:
                time.sleep(2)
                download = chrome.find_element_by_xpath('//*[@id="divArquivos"]/div[2]/table/tbody/tr/td[7]/div')
                time.sleep(2)
                download.click()
                print('Cliquei no download!')
                print('Download concluido!')
                time.sleep(3)
                chrome.close()
                return 1
            except:
                print('Attempting to click at the download icon')
            
        time.sleep(5)
    return 0

def extract_data_from_sistax_sheet(log=False):
    function_name = extract_data_from_sistax_sheet.__name__
    logger(function_name, msg='START')
    arr_new_sistax_sheet = []
    arr_new_egs_sheet1 = [] # used in padronizacao_de_regras_fiscais
    arr_new_egs_sheet2 = [] # used in cadastro_de_produtos
    path_sistax_sheet = scan_directory(path=config['paths']['input'], extension='csv')
    
    logger(function_name, msg=f'path_sistax_sheet = {path_sistax_sheet}')
    ## 'Master sheet' reading
    with open(path_sistax_sheet, mode='r+', encoding='UTF-8') as csv_file:
        csv_reader = csv.DictReader(csv_file,delimiter=';')
        line_count=1
        for row in csv_reader:
            line_count += 1
            ## CORREÇÃO DOS CAMPOS                                 
            if log:
                print(f'''--- [ BEFORE ]
                        linha: {line_count}
                        P:  {row['Código Produto']}     -> apenas capturar o valor
                        Q:  {row['Descrição']}     -> apenas capturar o valor
                        R:  {row['NCM']}     -> int to str
                        Y:  {row['ICMS-CST']}     -> apenas capturar o valor
                        AA: {row['ICMS-P-Red-BC']}     -> trocar ponto por virgula
                        AC: {row['ICMS-Aliquota']}     -> dividir por 10.000
                        AE: {row['ICMS-Valor Pauta']}     -> dividir por 10.000
                        AW: {row['ICMSST-P-Red-BC-ST']}     -> dividir por 10.000
                        AX: {row['ICMSST-Alíquota ST']}     -> dividir por 10.000
                        AZ: {row['ICMSST-MVA']}     -> trocar ponto por virgula
                        DM: {row['IPI-CST']}     -> apenas capturar o valor
                        DQ: {row['IPI-Alíquota']}     -> apenas capturar o valor
                        EK: {row['PIS-CST']}     -> apenas capturar o valor
                        EN: {row['PIS-Alíquota']}     -> dividir por 10.000
                        FG: {row['COFINS-CST']}     -> apenas capturar o valor
                        FJ: {row['COFINS-Alíquota']}     -> dividir por 10.000
                    ''')

             ## EXPORTAR UMA PLANILHA COM OS DADOS FILTRADOS E TRATADOS

            row['Código Produto']       = (str(row['Código Produto'])) if row['Código Produto'] else ''
            row['Descrição']            = (str(row['Descrição'])) if row['Descrição'] else '' 
            row['NCM']                  = (str(row['NCM'])) if row['NCM'] else ''
            row['ICMS-CST']             = (str(row['ICMS-CST'])) if row['ICMS-CST'] else ''  
            row['ICMS-P-Red-BC']        = (str(row['ICMS-P-Red-BC']).replace('.',',')) if row['ICMS-P-Red-BC'] else ''
            row['ICMS-Aliquota']        = (str(float(row['ICMS-Aliquota'])).replace('.',',')) if row['ICMS-Aliquota'] else ''
            row['ICMS-Valor Pauta']     = (str(float(row['ICMS-Valor Pauta'])).replace('.',',')) if row['ICMS-Valor Pauta'] else ''
            row['ICMSST-P-Red-BC-ST']   = (str(float(row['ICMSST-P-Red-BC-ST'])).replace('.',',')) if row['ICMSST-P-Red-BC-ST'] else ''
            row['ICMSST-Alíquota ST']   = (str(float(row['ICMSST-Alíquota ST'])).replace('.',',')) if row['ICMSST-Alíquota ST'] else ''
            row['ICMSST-MVA']           = (str(row['ICMSST-MVA']).replace('.',',')) if row['ICMSST-MVA'] else ''
            row['IPI-CST']              = (str(row['IPI-CST'])) if row['IPI-CST'] else ''
            row['IPI-Alíquota']         = (str(row['IPI-Alíquota'])) if row['IPI-Alíquota'] else ''
            row['PIS-CST']              = (str(row['PIS-CST'])) if row['PIS-CST'] else ''
            row['PIS-Alíquota']         = (str(float(row['PIS-Alíquota'])).replace('.',',')) if row['PIS-Alíquota'] else ''
            row['COFINS-CST']           = (str(row['COFINS-CST'])) if row['COFINS-CST'] else ''
            row['COFINS-Alíquota']      = (str(float(row['COFINS-Alíquota'])).replace('.',',')) if row['COFINS-Alíquota'] else ''

            if log:
                print(f'''--- [ AFTER ]
                        linha: {line_count},
                        P:  {row['Código Produto']} 
                        Q:  {row['Descrição']} 
                        R:  {row['NCM']} 
                        Y:  {row['ICMS-CST']}
                        AA: {row['ICMS-P-Red-BC']} 
                        AC: {row['ICMS-Aliquota']}  
                        AE: {row['ICMS-Valor Pauta']} 
                        AW: {row['ICMSST-P-Red-BC-ST']} 
                        AX: {row['ICMSST-Alíquota ST']}
                        AZ: {row['ICMSST-MVA']} 
                        DM: {row['IPI-CST']}
                        DQ: {row['IPI-Alíquota']} 
                        EK: {row['PIS-CST']} 
                        EN: {row['PIS-Alíquota']} 
                        FG: {row['COFINS-CST']} 
                        FJ: {row['COFINS-Alíquota']} 
                    ''')

            new_sistax_sheet_line = {'Código Produto':row['Código Produto'],
                              'Descrição':row['Descrição'],             
                              'NCM':row['NCM'],                  
                              'ICMS-CST':row['ICMS-CST'],               
                              'ICMS-P-Red-BC':row['ICMS-P-Red-BC'],        
                              'ICMS-Aliquota':row['ICMS-Aliquota'],        
                              'ICMS-Valor Pauta':row['ICMS-Valor Pauta'],     
                              'ICMSST-P-Red-BC-ST':row['ICMSST-P-Red-BC-ST'],   
                              'ICMSST-Alíquota ST':row['ICMSST-Alíquota ST'],   
                              'ICMSST-MVA':row['ICMSST-MVA'],           
                              'IPI-CST':row['IPI-CST'],              
                              'IPI-Alíquota':row['IPI-Alíquota'],         
                              'PIS-CST':row['PIS-CST'],              
                              'PIS-Alíquota':row['PIS-Alíquota'],         
                              'COFINS-CST':row['COFINS-CST'],           
                              'COFINS-Alíquota':row['COFINS-Alíquota']}
            arr_new_sistax_sheet.append(new_sistax_sheet_line)

            new_egs_sheet_line1 = {'CODNCM':row['NCM'],
                                'DESCRICAO':row['Descrição'],
                                'UFORIGEM':row['UF origem'],
                                'UFDESTINO':row['UF destino'],
                                'IPI_clEnq':'',
                                'ORIGEM':row['Origem'],
                                'IPI_CNPJProd':'',
                                'IPI_cSelo':'',
                                'IPI_qSelo':'',
                                'IPI_cEnq':999,
                                'IPI_CST':row['IPI-CST'],
                                'IPI_pIPI':row['IPI-Alíquota'],
                                'II_vBC':'',
                                'II_vDespAdu':'',
                                'II_vII':'',
                                'II_vIOF':'',
                                'PIS_CST':row['PIS-CST'],
                                'PIS_pPIS':row['PIS-Alíquota'],
                                'COFINS_CST':row['COFINS-CST'],
                                'COFINS_pCOFINS':row['COFINS-Alíquota'],
                                'ICMS_CST':row['ICMS-CST'],
                                'ICMS_modBC':'',
                                'ICMS_modBCST':'',
                                'ICMS_pRedBC':row['ICMS-P-Red-BC'],
                                'ICMS_pMVAST':'',
                                'ICMS_pRedBCST':'',
                                'ICMS_pICMS':'',
                                'ICMS_pICMSST':'',
                                'CFOP':row['CFOP'],
                                'InfoFisco':'',
                                'PFCP':row['ICMS-FCP'],
                                'PFCPST':'',
                                'PTRIBFEDERAL':'',
                                'PTRIBESTADUAL':'',
                                'PTRIBMUNICIPAL':'',
                                'CBENEF':'',
                                'ICMS_MOTDESICMS':'',
                                'CEST':row['ICMSST-Alíquota ST'],
                                'DESCRICAOCOMPLETA':'',
                                'ICMS_PICMSDIF':'',
                                'II_PII':'',
                                'II_PIOF':''}
            arr_new_egs_sheet1.append(new_egs_sheet_line1)

            new_egs_sheet_line2 = {'CODIGO':row['Código Produto'],
                                   'DESCRICAO':row['Descrição'],
                                   'NCM':row['NCM'],
                                   'CODUND':'Un',
                                   'CODUNDCOMPRA':'',
                                   'CODUNDVENDA':'',
                                   'IDCRESULTADO':'',
                                   'COEFICIENTE':'',
                                   'CONTROLAESTOQUE':'',
                                   'CODGRUPO':'',
                                   'CODFAMILIA':'',
                                   'IDCONTA':'',
                                   'PRECOUNIT':'',
                                   'DENSIDADE':'',
                                   'CPFCNPJFORNECEDOR':'',
                                   'PESO':'',
                                   'CODIGOEAN':row['EAN'],
                                   'PRECOCOMPRA':'',
                                   'ESPECIFICACOES':'',
                                   'COMPRIMENTO':'',
                                   'ALTURA':'',
                                   'LARGURA':'',
                                   'DIAMETRO':'',
                                   'ESTOQUEMINIMO':'',
                                   'ESTOQUEMAXIMO':'',
                                   'CODCFOP':'',
                                   'INTEGRAPDV':'',
                                   'INFORMAQTDPDV':'',
                                   'TIPOPROD':'',
                                   'IDCATEGORIAPROD':'',
                                   'IDMARCAPROD':'',
                                   'PRECOUNIT1':'',
                                   'ORIGEM':'',
                                   'CODIGOANP':'',
                                   'CODIGOAUX':'',
                                   'CEST':'',
                                   'QTDMINVENDA':'',
                                   'IDTIPOOP':'',
                                   'QTDMULTVENDA':'',
                                   'QTDCAIXA':'',
                                   'PESOCAIXA':''}
            arr_new_egs_sheet2.append(new_egs_sheet_line2)

    logger(function_name, msg='END')           
    return arr_new_sistax_sheet, arr_new_egs_sheet2 # ADD arr_new_egs_sheet1 to build padronizacao_de_regras_fiscais sheet info
            
def build_output_sheets(sistax_data, egs_data2):
    function_name = build_output_sheets.__name__
    logger(function_name, msg='START') 
    ## 'SISTAX sistax_sheet' generation
    path_output_sistax_csv = config['paths']['output'] + f"\{time.strftime('%d.%m.%Y')}-OutputSistaxSheet.csv"
    logger(function_name, msg=f'path_output_sistax_csv = {path_output_sistax_csv}') 
    logger(function_name, msg=f'{time.strftime("%d.%m.%Y")}-OutputSistaxSheet.csv" started ...') 

    with open(path_output_sistax_csv, mode='w', encoding='UTF-8',newline='') as csv_file:
        fieldnames = list(sistax_data[0].keys())
        writer = csv.DictWriter(csv_file, fieldnames=fieldnames,delimiter=';')
        writer.writeheader()
        for row in sistax_data:
            writer.writerow(row)
    print(f'Planilha gerada: {path_output_sistax_csv}')
    logger(function_name, msg=f'{time.strftime("%d.%m.%Y")}-OutputSistaxSheet.csv generation finished!')  

    # UNCOMENT TO BUILD THE PadronizacaoDeRegrasFiscais csv.csv        
    ## 'EGS padronizacao_de_regras_fiscais sheet' generation
    #path_output_egs_csv1 = config['paths']['output'] + f"\{time.strftime('%d.%m.%Y')}-PadronizacaoDeRegrasFiscais csv.csv"
    #logger(function_name, msg=f'path_output_egs_csv = {path_output_egs_csv1}') 
    #logger(function_name, msg=f'{time.strftime("%d.%m.%Y")}-PadronizacaoDeRegrasFiscais csv.csv generation started ...') 
    #
    #with open(path_output_egs_csv1, mode='w', encoding='UTF-8',newline='') as csv_file:
    #    fieldnames = list(egs_data1[0].keys())
    #    writer = csv.DictWriter(csv_file, fieldnames=fieldnames,delimiter=';')
    #    writer.writeheader()
    #    for row in egs_data1:
    #        writer.writerow(row)
    #logger(function_name, msg=f'{time.strftime("%d.%m.%Y")}--PadronizacaoDeRegrasFiscais csv.csv generation finished!')  
    #
    ### 'EGS - padronizacao_de_regras_fiscais xlsx sheet' generation
    #path_output_egs_xlsx1 = config['paths']['output'] + f"\{time.strftime('%d.%m.%Y')}-PadronizacaoDeRegrasFiscais xlsx.xlsx"
    #logger(function_name, msg=f'path_output_egs_xlsx1 = {path_output_egs_xlsx1}') 
    #logger(function_name, msg=f'{time.strftime("%d.%m.%Y")}-PadronizacaoDeRegrasFiscais xlsx.xlsx generation started ...') 
    #wb = Workbook()
    #ws = wb.active
    #with open(path_output_egs_csv1,mode='r',encoding='UTF-8') as f:
    #    for row in csv.reader(f,delimiter=';'):
    #        ws.append(row)
    #wb.save(path_output_egs_xlsx1)
    #logger(function_name, msg=f'{time.strftime("%d.%m.%Y")}-PadronizacaoDeRegrasFiscais xlsx.xlsx generation finished!')  

    ## 'EGS cadastro_de_produtos sheet' generation
    path_output_egs_csv2 = config['paths']['output'] + f"\{time.strftime('%d.%m.%Y')}-CadastroDeProdutos csv.csv"
    logger(function_name, msg=f'path_output_egs_csv2 = {path_output_egs_csv2}') 
    logger(function_name, msg=f'{time.strftime("%d.%m.%Y")}-CadastroDeProdutos csv.csv generation started ...') 

    with open(path_output_egs_csv2, mode='w', encoding='UTF-8',newline='') as csv_file:
        fieldnames = list(egs_data2[0].keys())
        writer = csv.DictWriter(csv_file, fieldnames=fieldnames,delimiter=';')
        writer.writeheader()
        for row in egs_data2:
            writer.writerow(row)
    logger(function_name, msg=f'{time.strftime("%d.%m.%Y")}-CadastroDeProdutos csv.csv generation finished!') 

    ## 'EGS - cadastro_de_produtos xlsx' generation
    path_output_egs_xlsx2 = config['paths']['output'] + f"\{time.strftime('%d.%m.%Y')}-CadastroDeProdutos xlsx.xlsx"
    logger(function_name, msg=f'path_output_egs_xlsx2 = {path_output_egs_xlsx2}') 
    logger(function_name, msg=f'{time.strftime("%d.%m.%Y")}-CadastroDeProdutos xlsx.xlsx generation started ...') 
    wb = Workbook()
    ws = wb.active
    with open(path_output_egs_csv2,mode='r',encoding='UTF-8') as f:
        for row in csv.reader(f,delimiter=';'):
            ws.append(row)
    wb.save(path_output_egs_xlsx2)
    logger(function_name, msg=f'{time.strftime("%d.%m.%Y")}-CadastroDeProdutos xlsx.xlsx generation finished!') 
    os.remove(path_output_egs_csv2)
    print(f'Planilha gerada: {path_output_egs_xlsx2}')
    print(f'Planilhas localizadas em: {config["paths"]["output"]}')
    logger(function_name, msg='END') 

def get_old_transfer_products_data(begin,end,warehouse_id):
    function_name = get_cur_transfer_products_data.__name__
    # TRANSFER PRODUCTS SHEET
    try:
        data = get_query('dwh',f"""select 
        	infos.transfer_id infos_transfer_id,
        	p.id prod_id,
        	p."name",
        	infos.quantity,
        	p.reference,
        	sp."cost",
        	infos.origin_warehouse_name origin,
        	infos.destination_name destiny,
        	cast(date(infos.ending_date) as text)  as ending_date
        from oltp.store_products sp
        inner join oltp.products p on p.id = sp.product_id 
        inner join (
        	select 
        		pts.id transfer_id
        		,text(jsonb_array_elements(pts.products)::json->>'reference') reference
        		,text(
        			jsonb_array_elements(
        				cast(jsonb_array_elements(pts.products)::json->'positions' as jsonb )
        			)::json->'quantity'
        		)  quantity
        		,pts.ending_date
        		,pts.origin_warehouse_id 
        		,pts.origin_warehouse_name 
        		,pts.destination_warehouse_id destination_id
        		,pts.destination_warehouse_name destination_name
        	from demanda.prod_transfers_sender pts 
        	where pts.origin_warehouse_id = '{warehouse_id}' --463
        		and pts.transfer_status != 'Cancelled'
                and pts.transfer_status != 'Piked'
        		and pts.starting_date::date >= date('{begin}') 
        		and pts.starting_date::date <= date('{end}') 
        ) infos on infos.reference = p.reference
        where p.country_id = 3
        and infos.destination_name not in('Cambio Mano a mano  - Leopoldina')
        order by infos_transfer_id asc
        ;""")

        key_transfer_id = list(set(data['infos_transfer_id']))
        transfer_id = list(data['infos_transfer_id'])
        prod_id = list(data['prod_id'])
        name = list(data['name'])
        quantity = list(data['quantity'])
        cost = list(data['cost'])
        origin = list(data['origin'])
        destiny = list(data['destiny'])
        ending_date = list(data['ending_date'])
        print(f'{len(key_transfer_id)} transferências encontradas!')

        for transfer in key_transfer_id:
            arr_sheet = []
            sheet_name = ''
            rows = transfer_id.count(transfer)
            for row in range(rows):
                index = transfer_id.index(transfer)

                str_origin = origin[index]
                str_destiny = destiny[index]
                ending = ending_date[index]
                sheet_name = f"[Realizada {ending}]_{str_origin}_para_{str_destiny}_TransferId_{transfer}"

                sheet_row = {'Codigo':prod_id[index],
                             'Quantidade':quantity[index],
                             'Unitario':cost[index],
                             'Descricao':name[index],                            
                             'UnidadeTributaria':'Un'
                             }
                # print(sheet_row)
                arr_sheet.append(sheet_row)

                transfer_id.pop(index)
                prod_id.pop(index)
                name.pop(index)
                quantity.pop(index)
                cost.pop(index)
                origin.pop(index)
                destiny.pop(index)
            
            ## 'DWH transfer sheet csv generation
            path_output_transfer_csv = config['paths']['output'] + f"\{time.time_ns()}.{time.strftime('%d.%m.%Y')}-TransferenciaDeProdutos csv.csv"
            time.sleep(0.2)
            logger(function_name, msg=f'path_output_transfer_csv = {path_output_transfer_csv}') 
            logger(function_name, msg=f'{time.strftime("%d.%m.%Y")}-TransferenciaDeProdutos csv.csv generation started ...') 
            with open(path_output_transfer_csv, mode='w', encoding='UTF-8',newline='') as csv_file:
                fieldnames = list(arr_sheet[0].keys())
                writer = csv.DictWriter(csv_file, fieldnames=fieldnames,delimiter=';')
                writer.writeheader()
                for row in arr_sheet:
                    writer.writerow(row)
            logger(function_name, msg=f'{time.strftime("%d.%m.%Y")}-TransferenciaDeProdutos csv.csv generation finished!') 

            ## 'DWH transfer sheet xlsx generation
            path_output_transfer_xlsx = config['paths']['output'] + f"\{sheet_name}.xlsx"
            logger(function_name, msg=f'path_output_transfer_xlsx = {path_output_transfer_xlsx}') 
            logger(function_name, msg=f'{time.strftime("%d.%m.%Y")}-{sheet_name}.xlsx generation started ...') 
            wb = Workbook()
            ws = wb.active
            with open(path_output_transfer_csv,mode='r',encoding='UTF-8') as f:
                for row in csv.reader(f,delimiter=';'):
                    ws.append(row)
            wb.save(path_output_transfer_xlsx)
            logger(function_name, msg=f'{time.strftime("%d.%m.%Y")}-{sheet_name}.xlsx generation finished!') 

            os.remove(path_output_transfer_csv)
            os.system('cls')
            print(f'Planilha gerada: {sheet_name}\n')
    except Exception as e:
        logger(function_name, msg='Não há nehuma transferência entre nodos para ser feita') 
        print('Não há nehuma transferência entre nodos para ser feita')
        print(e.with_traceback())
    if not Exception:
        print(f'Planilhas localizadas em: {config["paths"]["output"]}')

def get_cur_transfer_products_data(warehouse_id):
    function_name = get_cur_transfer_products_data.__name__
    # TRANSFER PRODUCTS SHEET
    try:
        data = get_query('dwh',f"""select 
        	infos.transfer_id infos_transfer_id,
        	p.id prod_id,
        	p."name" ,
        	infos.quantity,
        	p.reference ,
        	sp."cost" ,
        	infos.origin_warehouse_name origin,
        	infos.destination_name destiny,
            cast(date(infos.starting_date) as text) as starting_date
        from oltp.store_products sp
        inner join oltp.products p on p.id = sp.product_id 
        inner join (
        	select 
        		pts.id transfer_id
        		,text(jsonb_array_elements(pts.products)::json->>'reference') reference
        		,text(
        			jsonb_array_elements(
        				cast(jsonb_array_elements(pts.products)::json->'positions' as jsonb )
        			)::json->'quantity'
        		)  quantity
                ,pts.starting_date
        		,pts.origin_warehouse_id 
        		,pts.origin_warehouse_name 
        		,pts.destination_warehouse_id destination_id
        		,pts.destination_warehouse_name destination_name
        	from demanda.prod_transfers_sender pts 
        	where pts.origin_warehouse_id = '{warehouse_id}' --Vila Leopodina
        		and pts.transfer_status = 'Picked'
        ) infos on infos.reference = p.reference
        where p.country_id = 3
        and infos.destination_name not in('Cambio Mano a mano  - Leopoldina')
        order by infos_transfer_id asc
        ;""")

        key_transfer_id = list(set(data['infos_transfer_id']))
        transfer_id = list(data['infos_transfer_id'])
        prod_id = list(data['prod_id'])
        name = list(data['name'])
        quantity = list(data['quantity'])
        cost = list(data['cost'])
        origin = list(data['origin'])
        destiny = list(data['destiny'])
        start = list(data['starting_date'])
        print(f'{len(key_transfer_id)} transferências encontradas!')

        for transfer in key_transfer_id:
            arr_sheet = []
            sheet_name = ''
            rows = transfer_id.count(transfer)
            #print(rows)
            for row in range(rows):
                index = transfer_id.index(transfer)
                start_date = start[index]

                str_origin = origin[index]
                str_destiny = destiny[index]
                sheet_name = f"[Pendente {start_date}]_{str_origin}_para_{str_destiny}_TransferId_{transfer}"

                sheet_row = {'Codigo':prod_id[index],
                             'Quantidade':quantity[index],
                             'Unitario':cost[index],
                             'Descricao':name[index],                            
                             'UnidadeTributaria':'Un'
                             }
                # print(sheet_row)
                arr_sheet.append(sheet_row)

                transfer_id.pop(index)
                prod_id.pop(index)
                name.pop(index)
                quantity.pop(index)
                cost.pop(index)
                origin.pop(index)
                destiny.pop(index)
            
            ## 'DWH transfer sheet csv generation
            path_output_transfer_csv = config['paths']['output'] + f"\{time.time_ns()}.{time.strftime('%d.%m.%Y')}-TransferenciaDeProdutos csv.csv"
            time.sleep(0.2)
            logger(function_name, msg=f'path_output_transfer_csv = {path_output_transfer_csv}') 
            logger(function_name, msg=f'{time.strftime("%d.%m.%Y")}-TransferenciaDeProdutos csv.csv generation started ...') 
            with open(path_output_transfer_csv, mode='w', encoding='UTF-8',newline='') as csv_file:
                fieldnames = list(arr_sheet[0].keys())
                writer = csv.DictWriter(csv_file, fieldnames=fieldnames,delimiter=';')
                writer.writeheader()
                for row in arr_sheet:
                    writer.writerow(row)
            logger(function_name, msg=f'{time.strftime("%d.%m.%Y")}-TransferenciaDeProdutos csv.csv generation finished!') 

            ## 'DWH transfer sheet xlsx generation
            path_output_transfer_xlsx = config['paths']['output'] + f"\{sheet_name}.xlsx"
            logger(function_name, msg=f'path_output_transfer_xlsx = {path_output_transfer_xlsx}') 
            logger(function_name, msg=f'{time.strftime("%d.%m.%Y")}-{sheet_name}.xlsx generation started ...') 
            wb = Workbook()
            ws = wb.active
            #print(config['paths']['output'] + f"\{sheet_name}.xlsx")
            with open(path_output_transfer_csv,mode='r',encoding='UTF-8') as f:
                for row in csv.reader(f,delimiter=';'):
                    ws.append(row)
            wb.save(path_output_transfer_xlsx)
            logger(function_name, msg=f'{time.strftime("%d.%m.%Y")}-{sheet_name}.xlsx generation finished!') 

            os.remove(path_output_transfer_csv) # REMOVE TransferenciaDeProdutos csv.csv
            os.system('cls')
            print(f'Planilha gerada: {sheet_name}\n')
    except Exception as e:
        logger(function_name, msg='Não há nehuma transferência entre nodos para ser feita') 
        print('Não há nehuma transferência entre nodos para ser feita')
        #print(e)
    if not Exception:
        print(f'Planilhas localizadas em: {config["paths"]["output"]}')

def get_warehouses(country):
    """Returns:
    - key :warehouse's name
    - value : warehouse's id"""
    data = get_query('dwh',f"""select distinct
        w.id,
        w.warehouse
    from oltp.warehouses w
    inner join oltp.cities c on c.id = w.city_id 
    inner join oltp.countries c2 on c2.id = c.country_id 
    inner join (
        select distinct
            ws.warehouse_id 
        from oltp.warehouse_storages ws) ws on ws.warehouse_id = w.id
    where c2.country = '{country}'  
    ;""")

    # {warehouse:id}
    dict_warehouse = {}

    index = 0
    for warehouse in list(data['warehouse']):
        if 'mano' not in str(warehouse).lower():
            id_warehouse = data["id"][index]

            dict_warehouse[warehouse] = id_warehouse
        index += 1
    return dict_warehouse

class Tela1 :
    def __init__(self):
        today = time.strftime('%Y-%m-%d')
        
        self.dict_warehouse = get_warehouses('Brasil')
        dropdown = list(self.dict_warehouse.keys())

        frame_layout1 = [
            [sg.Text('Acessa a plataforme SISTAX e extrai a planilha geral de produtos com o IdCenario:2218069.',size=(90,1))],
            [sg.Text('Após finalizado o download, as planilhas de "CadastroDePordutos" e "OutputSistaxSheet" são compiladas.',size=(90,1))],
            [sg.Button('OPCAO 1',tooltip='Extração SISTAX...',size=(90,1))],
        ]
        frame_layout2 = [
            [sg.Text('Retorna as tranferências não finalizadas, aguardando emissão de NF de transferência.',size=(90,1))],
            [sg.Text('Armazém de origem: '),sg.InputOptionMenu([x for x in dropdown], size=(40,1), key='origin_opt2'),sg.Button('OPCAO 2',tooltip='Extração das tranferências a fazer...',size=(26,1))],
        ]
        frame_layout3 = [
            [sg.Text('Retorna transferências no período desejado.',size=(90,1))],
            [   sg.CalendarButton(button_text='Início',target='from_date',size=(22,1)),
                sg.In(today,key='from_date',size=(20,1)),
                sg.CalendarButton(button_text='Fim', target='to_date',size=(22,1)),
                sg.In(today,key='to_date',size=(20,1))
            ],
            [sg.Text('Armazém de origem: '),sg.InputOptionMenu([x for x in dropdown], size=(40,1), key='origin_opt3'),sg.Button('OPCAO 3',tooltip='Extração transferências anteriores...',size=(26,1))],
        ]
        #layout
        sg.theme('DarkGreen3')
        layout = [
            [sg.Frame('Extração SISTAX - Somente Brasil', frame_layout1, font='Any 12', title_color='#EC027A')],
            [sg.Frame('Extração de transferências a fazer', frame_layout2, font='Any 12', title_color='#EC027A')],
            [sg.Frame('Extração de transferências finalizadas', frame_layout3, font='Any 12', title_color='#EC027A')],
            [sg.Output(size=(90,6))]
        ]
        #Janela
        self.janela = sg.Window('Robô - Transferência de mercadorias', layout, font=('Helvetica'))

    def Iniciar(self):
        while True:
            self.event, self.values  = self.janela.Read()
            print('Selecionada: ',self.event)

            if self.event == sg.WIN_CLOSED:
                sys.exit()
                break

            if self.event == 'OPCAO 1':
                os.system('cls')
                time.sleep(1)
                # EXTRACTING DATA FROM SISTAX
                sistax_extraction =  download_sheet_from_sistax()
                while sistax_extraction != 1:
                    sistax_extraction = download_sheet_from_sistax()
                
                # CLEAN FOLDER / MOVE NEW SISTAX SHEET
                move_from_downloads_to_input()
                
                # PROCESSING THE SISTAX SHEET
                sistax_data, egs_data2 = extract_data_from_sistax_sheet(log=False)
                
                # BUILDING THE OUTPUT SHEETS
                build_output_sheets(sistax_data, egs_data2)
                print('Fim de execução!')              

            if self.event == 'OPCAO 2':
                os.system('cls')
                time.sleep(1)
                warehouse_id = str(self.dict_warehouse[self.values['origin_opt2']])
                get_cur_transfer_products_data(warehouse_id)
                print('Fim de execução!')      

            if self.event == 'OPCAO 3':
                os.system('cls')
                time.sleep(1)
                begin = self.values['from_date'][:10]
                end = self.values['to_date'][:10]
                warehouse_id = str(self.dict_warehouse[self.values['origin_opt3']])
                print('Início: ',begin)
                print('Fim: ',end)
                get_old_transfer_products_data(begin,end,warehouse_id)
                print('Fim de execução!')
                
screen = Tela1()
screen.Iniciar()   
